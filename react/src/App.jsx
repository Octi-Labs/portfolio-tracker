import React, { useState, useEffect } from 'react';
import { HashRouter as Router, Routes, Route } from "react-router-dom"; // Import HashRouter and rename it to Router
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Dashboard from './components/Dashboard';
import Wallets from './components/Wallets';
import Sentiment from './components/Sentiment';
import Settings from './components/Settings';

function App() {
  const [wallets, setWallets] = useState(() => {
    const savedWallets = JSON.parse(localStorage.getItem('wallets'));
    return savedWallets || [];
  });

  useEffect(() => {
    localStorage.setItem('wallets', JSON.stringify(wallets));
  }, [wallets]);

  return (
    <>
      <Router> {/* Use Router instead of BrowserRouter */}
        <Navbar />
        <Routes>
          <Route path="/" element={<Dashboard wallets={wallets} />} />
          <Route path="/wallets" element={<Wallets wallets={wallets} setWallets={setWallets} />} />
          <Route path="/sentiment" element={<Sentiment />} />
          <Route path="/settings" element={<Settings />} />
        </Routes>
        <Footer />
      </Router>
    </>
  );
}

export default App;
