import React from 'react';

const Footer = () => {

  return (
    <footer className="p-4 bg-white md:p-8 lg:p-10 dark:bg-gray-800">
      <div className="mx-auto max-w-screen-xl text-center">
      <span class="text-sm text-gray-500 sm:text-center dark:text-gray-400">🄯 2023-∞ - octilabs.org - open source software </span>
  </div>
</footer>


    );
};

export default Footer;