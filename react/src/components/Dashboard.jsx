import React, { useState, useEffect } from 'react';
import 'chart.js/auto';
import { Doughnut } from 'react-chartjs-2';
import axios from 'axios';
import coinData from './top_100_gecko.json';

const Dashboard = ({ wallets }) => {
  const [coinPrices, setCoinPrices] = useState({});
  const [totalUSDValue, setTotalUSDValue] = useState(0);
  const [lastUpdated, setLastUpdated] = useState(null);

  const fetchAndSaveCoinPrices = async () => {
    try {
      const coinIds = coinData.map(coin => coin.id);
      const coinIdsStr = coinIds.join(',');
      const url = "https://api.coingecko.com/api/v3/coins/markets";
      const params = {
        vs_currency: 'usd',
        ids: coinIdsStr
      };
      const response = await axios.get(url, { params });
      if (response.status === 200) {
        const prices = {};
        response.data.forEach((coin) => {
          prices[coin.id] = coin.current_price;
        });
        setCoinPrices(prices);
        setLastUpdated(new Date());
      }
    } catch (error) {
      console.error(`Failed to fetch data. Status code: ${error.response?.status || error.message}`);
    }
  };

  // Initial fetch when component mounts
  useEffect(() => {
    fetchAndSaveCoinPrices();
  }, []);

  // Interval-based fetch
  useEffect(() => {
    const intervalId = setInterval(() => {
      const now = new Date();
      if (!lastUpdated || (now - lastUpdated >= 2 * 60 * 1000)) {
        fetchAndSaveCoinPrices();
      }
    }, 10 * 1000);

    return () => clearInterval(intervalId);
  }, [lastUpdated]);

  useEffect(() => {
    let totalValue = 0;
    wallets.forEach((wallet) => {
      totalValue += wallet.balance * (coinPrices[wallet.currency] || 0);
    });
    setTotalUSDValue(totalValue);
  }, [coinPrices, wallets]);

  const chartData = {
    labels: wallets.map((wallet) => wallet.currency.toUpperCase()),
    datasets: [{
      data: wallets.map((wallet) => wallet.balance * (coinPrices[wallet.currency] || 0)),
      backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
    }],
  };

  const config = {
    type: 'doughnut',
    data: chartData,
  };

  return (
    <div className="dark bg-gray-800 min-h-screen min-w-screen text-white">
      <h1 className="text-center text-3xl font-bold py-5">Wallet Dashboard</h1>
      <div className="flex flex-col md:flex-row justify-center items-center">
        <div className="flex flex-col items-center w-full md:w-2/3">
          {wallets.map((wallet) => (
            <div key={wallet.id} className="bg-gray-700 p-4 rounded my-2 w-2/3 flex justify-between mx-auto">
              <div>
                <h2 className="font-bold">{wallet.label}</h2>
                <p>Currency: {wallet.currency}</p>
                <p>Balance: {wallet.balance} {wallet.currency.toUpperCase()}</p>
                <p>Current Price: ${coinPrices[wallet.currency] || 'Loading...'}</p>
                <p>Balance Value: ${(wallet.balance * (coinPrices[wallet.currency] || 0)).toFixed(2) || 'Loading...'}</p>
              </div>
            </div>
          ))}
        </div>
        <div className="w-full md:w-1/6 mt-4 md:mt-0">
          <h2 className="text-4xl text-green-500 mb-8">Total USD Value: ${totalUSDValue.toFixed(2)}</h2>
          <Doughnut {...config} width={200} height={200} />
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
