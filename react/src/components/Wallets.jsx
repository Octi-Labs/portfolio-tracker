import React, { useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import currenciesData from './top_100_gecko.json'; // Import the JSON file

const Wallets = ({ wallets, setWallets }) => {
  const [walletLabel, setWalletLabel] = useState('');
  const [walletCurrency, setWalletCurrency] = useState('');
  const [walletBalance, setWalletBalance] = useState('');
  const [editingWallet, setEditingWallet] = useState(null);
  const [currencies, setCurrencies] = useState([]);

  useEffect(() => {
    // Set the currencies from the imported JSON file
    setCurrencies(currenciesData);
  }, []);


  const addOrUpdateWallet = (e) => {
    e.preventDefault();

    if (editingWallet) {
      const updatedWallets = wallets.map(wallet =>
        wallet.id === editingWallet.id ? { ...wallet, label: walletLabel, currency: walletCurrency, balance: walletBalance } : wallet
      );
      setWallets(updatedWallets);
      setEditingWallet(null);
    } else {
      const newWallet = {
        id: uuidv4(),
        label: walletLabel,
        currency: walletCurrency,
        balance: walletBalance,
      };

      setWallets([...wallets, newWallet]);
    }
    setWalletLabel('');
    setWalletCurrency('');
    setWalletBalance('');
  };

  const deleteWallet = (id) => {
    const updatedWallets = wallets.filter(wallet => wallet.id !== id);
    setWallets(updatedWallets);
  };

  const startEditing = (wallet) => {
    setEditingWallet(wallet);
    setWalletLabel(wallet.label);
    setWalletAddress(wallet.address);
  };

  return (
    <div className="dark bg-gray-800 min-h-screen min-w-screen text-white">
      <h1 className="text-center text-3xl font-bold py-5">Crypto Wallets</h1>
      <form onSubmit={addOrUpdateWallet} className="flex flex-col items-center">
        <input
          type="text"
          value={walletLabel}
          onChange={(e) => setWalletLabel(e.target.value)}
          placeholder="Wallet Label"
          className="my-2 bg-gray-700 text-white p-2 rounded w-1/3"
        />
        <select
          value={walletCurrency}
          onChange={(e) => setWalletCurrency(e.target.value)}
          className="my-2 bg-gray-700 text-white p-2 rounded w-1/3"
        >
          <option value="" disabled>Select Currency</option>
          {currencies.map((currency) => (
            <option key={currency.id} value={currency.id}>
              {currency.name}
            </option>
          ))}
        </select>
        <input
          type="number"
          step="0.000000000001"
          value={walletBalance}
          onChange={(e) => setWalletBalance(e.target.value)}
          placeholder="Balance"
          className="my-2 bg-gray-700 text-white p-2 rounded w-1/3"
        />
        <button type="submit" className="my-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
          {editingWallet ? 'Update Wallet' : 'Add Wallet'}
        </button>
      </form>
      <div className="flex flex-col items-center">
        {wallets.map((wallet) => (
          <div key={wallet.id} className="bg-gray-700 p-4 rounded my-2 w-1/3 flex justify-between">
            <div>
              <h2 className="font-bold">{wallet.label}</h2>
              <p>{wallet.address}</p>
            </div>
            <div>
              <button onClick={() => startEditing(wallet)} className="bg-orange-500 hover:bg-orange-700 text-white font-bold py-2 px-4 rounded mr-2">
                Edit
              </button>
              <button onClick={() => deleteWallet(wallet.id)} className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                Delete
              </button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Wallets;
