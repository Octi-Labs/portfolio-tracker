// Import the required modules
import axios from 'axios';
import fs from 'fs';

export const collectPrices = () => {

// Read the coin data from the JSON file
fs.readFile('top_100_gecko.json', 'utf8', (err, data) => {
  if (err) {
    console.error("Failed to read JSON file:", err);
    return;
  }

  const coinData = JSON.parse(data);

  // Extract the IDs from the coin data
  const coinIds = coinData.map(coin => coin.id);

  // Convert the list of IDs to a comma-separated string
  const coinIdsStr = coinIds.join(',');

  // Define the API endpoint
  const url = "https://api.coingecko.com/api/v3/coins/markets";

  // Define the parameters
  const params = {
    vs_currency: 'usd',
    ids: coinIdsStr  // Use the IDs from the JSON file
  };

  // Make the API call
  axios.get(url, { params })
    .then(response => {
      // Save the data to a JSON file
      fs.writeFile('coin_prices.json', JSON.stringify(response.data), err => {
        if (err) {
          console.error("Failed to write JSON file:", err);
        }
      });
    })
    .catch(error => {
      console.error(`Failed to fetch data. Status code: ${error.response.status}`);
    });
});

};