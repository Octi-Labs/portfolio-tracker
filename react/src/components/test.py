import requests
import json

# Read the coin data from the JSON file
with open('top_100_gecko.json', 'r') as f:
    coin_data = json.load(f)

# Extract the IDs from the coin data
coin_ids = [coin['id'] for coin in coin_data]

# Convert the list of IDs to a comma-separated string
coin_ids_str = ','.join(coin_ids)

# Define the API endpoint
url = "https://api.coingecko.com/api/v3/coins/markets"

# Define the parameters
params = {
    'vs_currency': 'usd',
    'ids': coin_ids_str  # Use the IDs from the JSON file
}

# Make the API call
response = requests.get(url, params=params)

# Check if the request was successful
if response.status_code == 200:
    # Parse the JSON response
    data = response.json()

    # Save the data to a JSON file
    with open('coin_prices.json', 'w') as f:
        json.dump(data, f)
else:
    print(f"Failed to fetch data. Status code: {response.status_code}")
