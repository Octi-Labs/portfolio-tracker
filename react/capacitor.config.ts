import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.octilabsportfoliotracker.app',
  appName: 'portfoliotracker',
  webDir: 'dist',
  server: {
    androidScheme: 'http'
  }
};

export default config;
